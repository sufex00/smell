from sklearn.model_selection import StratifiedShuffleSplit
from sklearn import preprocessing 
import time

import pandas as pd
from dml import NCA, kNN, MultiDML_kNN

from dml.anmm import ANMM, KANMM
from dml.llda import KLLDA
from dml.base import Euclidean
from dml.dmlmj import DMLMJ
from dml.dmlmj import KDMLMJ
from dml.itml import ITML

from dml.kda import KDA

from dml.lda import LDA

from dml.ldml import LDML

from dml.llda import LLDA

from dml.lmnn import KLMNN
from dml.lmnn import LMNN

from dml.lsi import LSI

from dml.mcml import MCML

from dml.ncmc import NCMC
from dml.ncmc import NCMC_Classifier

from dml.ncmml import NCMML
from dml.pca import PCA

from dml.base import Covariance

import numpy as np

from sklearn.metrics import accuracy_score
from sklearn.preprocessing import MinMaxScaler

np.random.seed(1234)

print('Carregando dataset')

def start_db(name):
    f = open(name)
    line = f.readline()
    l = 0
    while line: 
        l += 1
        if line[0] is '@':
            line = f.readline()
        else:
            return l-1

def load_data(name, k_fold, dic = '../../db/'):
    name_tra = dic+name+'/'+name+'-10-'+str(k_fold)+'tra.dat'
    name_tst = dic+name+'/'+name+'-10-'+str(k_fold)+'tst.dat'

    data_tra = pd.read_csv(name_tra, sep = ',', header = None, skiprows = start_db(name_tra))
    data_tst = pd.read_csv(name_tst, sep = ',', header = None, skiprows = start_db(name_tst)) 
#
    X_train = data_tra.as_matrix()[:, : -1]
    X_test = data_tst.as_matrix()[:, : -1]
#
    Y_train = data_tra.as_matrix()[:, -1]
    Y_test = data_tst.as_matrix()[:, -1]
#
    label_encoder = preprocessing.LabelEncoder() 
    Y_train = label_encoder.fit_transform(Y_train)
    Y_test  = label_encoder.fit_transform(Y_test)
#
    return np.array(X_train), np.array(Y_train), np.array(X_test), np.array(Y_test), label_encoder

def metrics(y_pred, labels):
    return accuracy_score(labels, y_pred)
    
def to_string(name, method, f):
    f.write('------------------------------------------------------------------------------\n')
    f.write(str('Methods : '+ name+ '\n'))
    f.write(str('Train : '+ str(sum(method[1])/10.0)+ '('+ str(np.std(method[1])) +')'  +'\nlist : '+ str(method[1])+'\n\n' ))
    f.write(str('Test  : '+ str(sum(method[2])/10.0) + '('+ str(np.std(method[2])) +')' + '\nlist : '+ str(method[2])+'\n\n' ))

name = 'monk-2'

neighbors = 3

r = []
r_train = []

methods = {
		'ANMM':[ANMM(n_friends = 10, n_enemies = 10), [], []],
##                'KANMM':[KANMM(), [], []],
##                'KLLDA':[KLLDA(), [], []], 
##                'Mahalanobis':[Covariance(), [], []],
		'Euclidean' :[Euclidean(), [], []],
		'DMLMJ' : [DMLMJ(n_neighbors = neighbors), [], []],
		'KDMLMJ' : [KDMLMJ(), [], []],
		'ITML' :[ITML(), [], []],
		'KDA' : [KDA(), [], []],
		'LDA' : [LDA(), [], []],
#		'LDML' : [LDML(b = 'constant'), [], []],
		'LLDA' : [LLDA(), [], []],
##		'KLMNN' : [KLMNN(k = neighbors), [], []],
		'LMNN' : [LMNN(k = neighbors), [], []],
#		'MCML' : [MCML(), [], []],
		'LSI': [LSI(supervised = True), [], []],
		'NCMC' : [NCMC(centroids_num = neighbors), [], []],
##		'NCMC_Classifier' : [NCMC_Classifier(centrois_num = neighbors), [], []],
##		'NCMML' : [NCMML(), [], []], 
		'PCA' : [PCA(), [], []],
		'NCA': [NCA() , [], []]
          }


k = 1

from sklearn.model_selection import StratifiedKFold

skf = StratifiedKFold(n_splits = 10, random_state = np.random)
_, _, x, y, _ = load_data(name, k)

for k in range(1, 11):
#for train_index, test_index in skf.split(x, y):
    scaler = MinMaxScaler()

    X_train, Y_train, x_test, y_test, l_encoder = load_data(name, k)

#    X_train, x_test = x[train_index], x[test_index]
#    Y_train, y_test = y[train_index], y[test_index]

#    scaler.fit(np.concatenate((X_train, x_test)))
#    X_train = scaler.transform(X_train)
#    x_test = scaler.transform(x_test)

    X = np.concatenate((X_train, x_test))
    print('X = ', X.shape)
    print('X_train = ', X_train.shape)
    print('Y_train = ', Y_train.shape)
    print('X_test = ', x_test.shape)
    print('Y_test = ', y_test.shape)    
    print('k = ', k)

    #time.sleep(5)

    dml = 0

    for m in methods:
        print(m)
        dml = methods[m]
  
        knn = kNN(n_neighbors=neighbors,dml_algorithm=dml[0])

        dml[0].fit(X_train,Y_train)
        knn.fit(X_train,Y_train)

        dml[2].append(metrics(knn.predict(x_test), y_test))
        dml[1].append(metrics(knn.predict(X_train), Y_train))

f = open('result-'+name+'-neighbors='+str(neighbors)+'.dat', 'w+')
for m in methods:
    to_string(m, methods[m], f)
f.close()



