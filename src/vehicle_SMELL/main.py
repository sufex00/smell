# -*- coding: utf-8 -*-
"""

"""

import warnings
import numpy as np
import keras
from keras_smell import SMELL
import os
import sys
from sklearn.preprocessing import MinMaxScaler

np.random.seed(9)
warnings.filterwarnings('ignore')

# os.environ["CUDA_VISIBLE_DEVICES"]='0' # choose GPU 0

exec(open("./preparation.py").read())                 # Read dataset Monk-2

name = 'vehicle'
X_train, Y_train, x_test, y_test, l_encoder = load_data(name)#, dic = './') # Dic = Dataset Directory


scaler = MinMaxScaler()                                # Min-Max normalization
scaler.fit(np.concatenate((X_train, x_test)))
X_train = scaler.transform(X_train)
x_test = scaler.transform(x_test)

X = np.concatenate((X_train, x_test))

print('Data information')
print('X = ', X.shape)
print('X_train = ', X_train.shape)
print('Y_train = ', Y_train.shape)
print('X_test = ', x_test.shape)
print('Y_test = ', y_test.shape)

# Auxiliary vector for training step

v_train = get_data(X_train, Y_train)
v_test = get_data(x_test, y_test)

print('Auxiliary vector')               
#print (v_train[0])   # All class 0 samples are in v_train[0]
#print (v_train[1])   # All class 1 samples are in v_train[1]
#print (v_train[2])   # All class 2 samples are in v_train[2]
#   .
#   .
#   .
#print (v_train[n])   # All class 0 samples are in v_train[n]

NUM_MARKERS = 5
POS_MARKERS = 3
R_D = 0
R_KL = 1
R_R = 0.001

AUTOENCODER_TRAINING_EPOCH = 40
TOTAL_EPOCH = 1.01e4
NUMBER_NEIGHBOUR = 3

c = SMELL(n_clusters=NUM_MARKERS, pos_markers = POS_MARKERS, 
                            r_kl = R_KL, r_r = R_R, 
                            input_dim=X_train.shape[1], 
                            autoencoder_training = AUTOENCODER_TRAINING_EPOCH,
                            verbose = 0) # changer verbose = 1 for print autoencoder epochs
c.initialize(X, v_train = v_train)

#c.autoencoder_summary()

#c.SMELL_summary()

c.training(v_train, v_test,  N = 400, update_interval=900,
                save_interval=1000, iter_max=TOTAL_EPOCH, verbose = 1)

y_pred, acc = c.predict_knn(X_train, Y_train, x_test, y_test, k = NUMBER_NEIGHBOUR)

print('Test dataset:')
print('Acc = ', acc)
print('Predict (SMELL) = ', y_pred)
print('Predict (real)  = ', list(y_test))
