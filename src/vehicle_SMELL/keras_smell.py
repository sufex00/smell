


'''

Definition can accept somewhat custom neural networks. Defaults are from paper.
'''
import sys
import numpy as np
import keras.backend as K
import tensorflow as tf
from keras.initializers import RandomNormal
from keras.engine.topology import Layer, InputSpec
from keras.models import Model, Sequential
from keras.layers import Dense, Dropout, Input, Lambda, Conv1D, MaxPooling1D, Flatten, UpSampling1D
from keras.layers import Reshape, Multiply, Add, Conv2D, MaxPooling2D, UpSampling2D
import keras.regularizers as regularizers
import keras.initializers as initializers
from keras.regularizers import l2, Regularizer
from keras.optimizers import SGD
from sklearn.preprocessing import normalize
from keras.callbacks import LearningRateScheduler
from sklearn.cluster import KMeans
from sklearn.decomposition import PCA
if (sys.version[0] == 2):
    import cPickle as pickle
else:
    import pickle
import numpy as np
import time
import numpy.random as rng

from sklearn.metrics import accuracy_score
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from sklearn.metrics import f1_score
from sklearn.metrics import cohen_kappa_score
from sklearn.metrics import roc_auc_score
from sklearn.metrics import confusion_matrix

import tqdm
from tqdm import *


def euclidean_distance(vects):
    x, y = vects
    sum_square = K.sum(K.square(x - y), axis=1, keepdims=True)
    return K.sqrt(K.maximum(sum_square, K.epsilon()))


def eucl_dist_output_shape(shapes):
    shape1, shape2 = shapes
    return (shape1[0], 1)

def contrastive_loss(y_true, y_pred):
    '''Contrastive loss from Hadsell-et-al.'06
    http://yann.lecun.com/exdb/publis/pdf/hadsell-chopra-lecun-06.pdf
    '''
    margin = 1
    square_pred = K.square(y_pred)
    margin_square = K.square(K.maximum(margin - y_pred, 0))
    return K.mean(y_true * square_pred + (1 - y_true) * margin_square)


def label_ohe(s):
    y_pred = []
    
    label = set()
    for e in s:
        label.add(e)

    for idx, e in enumerate(s):
        aux = np.zeros(len(label))
        aux[e] = 1
        y_pred.append(aux)
    return np.array(y_pred)


def W_init(shape, name=None):
        """Initialize weights as in paper"""
        values = rng.normal(loc=0, scale=1e-2, size=shape)
        return K.variable(values, name=name)

    # //TODO: figure out how to initialize layer biases in keras.
def b_init(shape, name=None):
        """Initialize bias as in paper"""
        values = rng.normal(loc=0.5, scale=1e-2, size=shape)
        return K.variable(values, name=name)

def siam(autoencoder, encoder, cluster, input_shape):

    left_input = Input(shape = (input_shape,))
    right_input = Input(shape = (input_shape,))

    # build convnet to use in each siamese 'leg'

    encoded_l = encoder(left_input)
    encoded_r = encoder(right_input)

    autoencoded_l = autoencoder(left_input)
    autoencoded_r = autoencoder(right_input)
    # merge two encoded inputs with the l1 distance between them
    L1_distance = lambda x: K.abs(x[0] - x[1])

    L1_layer = Lambda(lambda tensors:K.abs(tensors[0] - tensors[1]))
    L1_distance = L1_layer([encoded_l, encoded_r])
    
    distance = Lambda(euclidean_distance,
                  output_shape=eucl_dist_output_shape)([encoded_l, encoded_r])

    DEC = Model(inputs=[left_input, right_input], 
                 outputs=[cluster(L1_distance), distance ,autoencoded_l, autoencoded_r])
    encoder_layer = Model(inputs = [left_input, right_input], 
                           outputs = L1_distance)
    return DEC, encoder_layer


def encoder_layer(input_size):

    encoding_img = 64

    input_img = Input(shape=(input_size,))
    encoded = Dense(512, activation = 'relu')(input_img)
    encoded = Dense (512, activation = 'relu')(encoded)
    encoded = Dense(2048, activation = 'relu')(encoded)
    encoded = Dense(encoding_img, activation = 'relu')(encoded)
    decoded = Dense(2048, activation = 'relu')(encoded)
    decoded = Dense(512, activation = 'relu')(decoded)
    decoded = Dense(512, activation = 'relu') (decoded)
    decoded = Dense(input_size, activation = 'sigmoid')(decoded)


    autoencoder = Model(input_img, decoded)

    encoder = Model(input_img, encoded)
    return autoencoder, encoder

def get_siam_half(X, num):
    cancer = []
    no_cancer = []
    label= []
    v = [[], []]
    label = []
    for e in range(num):
         if e < num/2.0:
             r = np.random.randint(X.shape[0])
             elem = np.random.randint(X[r].shape[0])
             v[0].append(X[r][elem])
             elem = np.random.randint(X[r].shape[0])
             v[1].append(X[r][elem])
             label.append(0)
         else:
             r = np.random.randint(X.shape[0])
             elem = np.random.randint(X[r].shape[0])
             v[0].append(X[r][elem])
             r1 = r
             while r1 == r:
                 r1 = np.random.randint(X.shape[0])
             elem1 = np.random.randint(X[r1].shape[0])
             v[1].append(X[r1][elem1])
             label.append(1)
    return(np.array(v[0]), np.array(v[1]), np.array(label))
 
def distance(x, num_pos, N):
        import math 
        regularization = 0.
        l = 0.1
        if l:

            for e in range(num_pos):
                distance_2 = K.sum(K.abs(x[e] - x[e+1])**2)
                #distance_2 = K.min(K.abs(x[e] - x[e+1]))
                regularization += l * 1/(distance_2+0.001)
            for e in range(num_pos, N-1):
                distance_2 = K.sum(K.abs(x[e] - x[e+1])**2)
                #distance_2 = K.min(K.abs(x[e] - x[e+1]))
                regularization += l * 1/(distance_2 + 0.001)

            regularization /= (math.factorial(int(N/2))/math.factorial(N-2))            
        return regularization

class MarkerLayer(Layer):
    '''
    Marker layer which converts latent space Z of input layer
    into a probability vector for each cluster defined by its centre in
    Z-space. Use Kullback-Leibler divergence as loss, with a probability
    target distribution.
    # Arguments
        output_dim: int > 0. Should be same as number of clusters.
        input_dim: dimensionality of the input (integer).
            This argument (or alternatively, the keyword argument `input_shape`)
            is required when using this layer as the first layer in a model.
        weights: list of Numpy arrays to set as initial weights.
            The list should have 2 elements, of shape `(input_dim, output_dim)`
            and (output_dim,) for weights and biases respectively.
        alpha: parameter in Student's t-distribution. Default is 1.0.
    # Input shape
        2D tensor with shape: `(nb_samples, input_dim)`.
    # Output shape
        2D tensor with shape: `(nb_samples, output_dim)`.
    '''
    def __init__(self, output_dim, input_dim=None, pos_markers = 3, markers = 4, weights=None, alpha=1.0, kernel_regularizer = None, distance = 8,**kwargs):
        self.output_dim = 2##output_dim
        self.input_dim = input_dim
        self.alpha = alpha
        self.encoding_dim = 6
        self.distance = distance
        # kmeans cluster centre locations
        self.initial_weights = weights
        self.input_spec = [InputSpec(ndim=2)]
        self.kernel_regularizer = regularizers.get(kernel_regularizer)

        self.pos_markers = pos_markers
        self.markers = markers

        if self.input_dim:
            kwargs['input_shape'] = (self.input_dim,)
        super(MarkerLayer, self).__init__(**kwargs)

    def build(self, input_shape):
        assert len(input_shape) == 2
        input_dim = input_shape[1]
        self.input_spec = [InputSpec(dtype=K.floatx(),
                                     shape=(None, input_dim))]

        self.W = K.variable(self.initial_weights)
        self._trainable_weights = [self.W]

    def call(self, x, mask=None):
        num_pos = self.pos_markers  #number of positive atractor
        q = 1.0/(1.0 + K.sqrt(K.sum(K.square(K.expand_dims(x, 1) - self.W), axis=2))**2 /self.alpha)
        q = q**((self.alpha+1.0)/2.0)
        q = K.transpose(K.transpose(q)/K.sum(q, axis=1))

        pos = K.sum(q[:, :num_pos], axis = 1)
        neg = K.sum(q[:, num_pos:], axis = 1)

        q_new = K.concatenate([pos, neg])
        q_new = K.transpose(K.reshape(q_new, [2, -1]))
        
        self.add_loss(distance(self.W, num_pos, self.markers))

        return q_new

    def get_output_shape_for(self, input_shape):
        assert input_shape and len(input_shape) == 2
        return (input_shape[0], self.output_dim)

    def compute_output_shape(self, input_shape):
        assert input_shape and len(input_shape) == 2
        return (input_shape[0], self.output_dim)

    def get_config(self):
        config = {'output_dim': self.output_dim,
                  'input_dim': self.input_dim}
        base_config = super(MarkerLayer, self).get_config()
        return dict(list(base_config.items()) + list(config.items()))


class SMELL(object):
    def __init__(self,
                 n_clusters,
                 pos_markers,
                 input_dim,
                 r_kl=1,
                 r_r = 0.001,
                 encoded=None,
                 decoded=None,
                 alpha=1.0,
                 autoencoder_training = 400,
                 pretrained_weights=None,
                 cluster_centres=None,
                 batch_size=24,
                 verbose = 0,
                 **kwargs):

        super(SMELL, self).__init__()

        self.n_clusters = n_clusters
        self.input_dim = input_dim
        self.encoded = encoded
        self.decoded = decoded
        self.alpha = alpha
        self.pretrained_weights = pretrained_weights
        self.cluster_centres = cluster_centres
        self.batch_size = batch_size
        self.autoencoder_training = autoencoder_training
        self.learning_rate = 0.1
        self.iters_lr_update = 20000
        self.lr_change_rate = 0.1
        self.verbose = verbose

        self.pos_markers = pos_markers
        self.r_kl= r_kl
        self.r_r = r_r

        # greedy layer-wise training before end-to-end training:

        self.encoders_dims = [self.input_dim, 2]

        self.input_layer = Input(shape=(self.input_dim,), name='input')
        dropout_fraction = 0.2
        init_stddev = 0.01

        self.layer_wise_autoencoders = []
        self.encoders = []
        self.decoders = []
        self.autoencoder, self.encoder = encoder_layer(self.input_dim,)
        self.autoencoder.compile(loss='mse', optimizer=SGD(lr=self.learning_rate, decay=0, momentum=0.9))

        # build the end-to-end autoencoder for finetuning
        # Note that at this point dropout is discarded
        if cluster_centres is not None:
            assert cluster_centres.shape[0] == self.n_clusters
            assert cluster_centres.shape[1] == self.encoder.layers[-1].output_dim

        if self.pretrained_weights is not None:
            self.autoencoder, self.encoder = encoder_layer(self.input_dim,)
            self.autoencoder.load_weights(self.pretrained_weights)

    def p_mat(self, q):
        weight = q**2 / q.sum(0)
        return (weight.T / weight.sum(1)).T

    def initialize(self, X, save_autoencoder=False, v_train = None):
        self.distance = distance
        if self.pretrained_weights is None:

            iters_per_epoch = int(len(X) / self.batch_size)

            print('layerwise pretrain')
            current_input = X
            lr_epoch_update = max(1, self.iters_lr_update / float(iters_per_epoch))
            
            def step_decay(epoch):
                initial_rate = self.learning_rate
                factor = int(epoch / lr_epoch_update)
                lr = initial_rate / (10 ** factor)
                return lr
            lr_schedule = LearningRateScheduler(step_decay)
            print('Finetuning autoencoder')
            
            #update encoder and decoder weights:
            self.autoencoder.fit(X, X, epochs=self.autoencoder_training, batch_size=6, shuffle=True, verbose = self.verbose)
            print('End Training (autoencoder)')
            if self.verbose != 0:
                self.autoencoder.summary()
            if save_autoencoder:
                self.autoencoder.save_weights('./autoencoder.h5')
        else:
            print('Loading pretrained weights for autoencoder.')
            self.autoencoder.load_weights(self.pretrained_weights)

        # update encoder, decoder
        
        for i in range(len(self.encoder.layers)):
            self.encoder.layers[i].set_weights(self.autoencoder.layers[i].get_weights())

        # initialize cluster centres using Lloyd
        print('Initializing cluster centres with Lloyd.')
        if self.cluster_centres is None:
            kmeans = KMeans(n_clusters=self.n_clusters, n_init=20)
            g = get_siam_half(v_train, 400)
            result = abs(self.encoder.predict(g[0]) - self.encoder.predict(g[1]))
            #print(result.shape)
            self.y_pred = kmeans.fit_predict(result)
            self.cluster_centres = kmeans.cluster_centers_

        self.DEC, self.encoder = siam(self.autoencoder, self.encoder, 
                MarkerLayer(self.n_clusters, pos_markers = self.pos_markers, markers = self.n_clusters, weights=self.cluster_centres,
                    name='makers')
                , self.input_dim)
        if self.verbose != 0:
            self.DEC.summary()


        self.DEC.compile(loss=['kullback_leibler_divergence', contrastive_loss, 'mse', 'mse'], loss_weights = 
[self.r_kl, 0.10, self.r_r, self.r_r],  optimizer = SGD(lr=0.01, momentum=0.9))
        return

    def autoencoder_summary(self):
        return self.autoencoder.summary()

    def SMELL_summary(self):
        return self.DEC.summary()

    def cluster_acc(self, y_true, y_pred):
        assert y_pred.size == y_true.size
        D = max(y_pred.max(), y_true.max())+1
        w = np.zeros((D, D), dtype=np.int64)
        for i in range(y_pred.size):
            w[y_pred[i], y_true[i]] += 1
        ind = linear_assignment(w.max() - w)
        return sum([w[i, j] for i, j in ind])*1.0/y_pred.size, w

    def training(self, v_train, v_test,  N=10, 
                tol=0.00, update_interval=900,
                iter_max=1e6,
                save_interval=1000,
                **kwargs):

        if update_interval is None:
            
            update_interval = v_train.shape[1]/self.batch_size
        print('Update interval', update_interval)

        if save_interval is None:
            
            save_interval = v_train.shape[1]/self.batch_size*50
        print('Save interval', save_interval)

        assert save_interval >= update_interval

        train = True
        iteration, index = 0, 0
        acc_m = 0.0
        self.accuracy = []
        ver = []
#        while train:
        #for p in tqdm(range(int(iter_max))):
        for p in range(int(iter_max)):
            sys.stdout.write('\r')
            # cutoff iteration
            if iter_max < iteration:
                print('Reached maximum iteration limit. Stopping training.')
                return self.y_pred

            # update (or initialize) probability distributions and propagate weight changes
            # from DEC model to encoder.
            if iteration % update_interval == 0:
                g = get_siam_half(v_test, N)
                self.q = self.DEC.predict([g[0], g[1]], verbose=0)
                loss_e = self.DEC.evaluate([g[0], g[1]], [label_ohe(g[2]), label_ohe(g[2])[:, 1], g[0], g[1]], verbose = 0)
#                print(self.q)
                self.q = self.q[0]
                self.p = label_ohe(g[2])
#                print(self.p)

                y_pred = self.q.argmax(1)

                sum_v = 0
                for idx in range(len(y_pred)):
                    if y_pred[idx] == self.y_pred[idx]:
                        sum_v += 1

                delta_label = (sum_v / y_pred.shape[0])
                if v_train is not None:
                    acc = accuracy_score(g[2], y_pred)#self.cluster_acc(g[2], y_pred)[0]
                    self.accuracy.append(acc)
                    if iteration % update_interval == 0:
                        print('Iteration '+str(iteration)+', Estimated Accuracy: '+str(np.round(acc, 5)) + ', Loss: '+str(np.round(loss_e[0], 5)))
                else:
                    print(str(np.round(delta_label*100, 5))+'% change in label assignment')

                if delta_label < tol:
                    print('Reached tolerance threshold. Stopping training.')
                    train = False
                    continue
                else:
                    self.y_pred = y_pred
                ver.append(acc) 
                if len(ver) > 30:
                    ver.pop(0)
                if False:#sum(ver) < 15:
                  if len(ver) == 30:
#                     train = False
                     result = abs(self.encoder.predict(g[0]) - self.encoder.predict(g[1]))
                     self.y_pred = kmeans.fit_predict(result)
                     self.cluster_centres = kmeans.cluster_centers_
                     self.DEC.layers[-2].set_weights([self.cluster_centres, self.cluster_center])
                     self.DEC.save('./DEC_model_.h5')
                     continue
                for i in range(len(self.encoder.layers)):
                    self.encoder.layers[i].set_weights(self.DEC.layers[i].get_weights())
                self.cluster_centres = self.DEC.layers[-3].get_weights()[0]


            # train on batch
            #sys.stdout.write('Iteration %d, ' % iteration)
            g = get_siam_half(v_train, self.batch_size)
            
            loss = self.DEC.train_on_batch([g[0], g[1]], [label_ohe(g[2]), g[0], g[1]])
            str_loss = 'Loss ' + str(loss)
            #sys.stdout.write(str_loss)
            if iteration % update_interval == 0:
                if self.verbose != 0:
                    print(str_loss)


            # save intermediate
            if iteration % save_interval == 0:
                g = get_siam_half(v_test, N)

                z = self.encoder.predict([g[0], g[1]])
                #score = self.DEC.evaluate([g[0], g[1]], [label_ohe(g[2]), g[0], g[1]])
                
                # save DEC model checkpoints
                #self.DEC.save('./DEC_model_.h5')
                if acc_m < loss[0]:
#                    self.DEC.save('./w/DEC_model_'+str(k)+'.h5')
                    acc_m = loss[0]

            iteration += 1
            sys.stdout.flush()
        return


    def predict_knn(self, x_train, y_train, x_test, y_test, k = 3, weight = None):
        
        if weight is not  None:
            self.DEC.load_weights(weight)
            for i in range(len(self.encoder.layers)):
                self.encoder.layers[i].set_weights(self.DEC.layers[i].get_weights())
        
        y_labels = set(y_train)
        y_pred = []

        for idx in range(len(x_test)):
            r = [[] for e in range(len(x_train))]
            predict = [x_test[idx] for e in range(len(x_train))]
            predict = np.array(predict)
            q = self.DEC.predict([x_train, predict])
            for e in range(len(x_train)):
                r[e] = [q[0][e][0], y_train[e]]
            r.sort(reverse = True)
            r = r[:k]
            s = np.zeros(len(y_labels))
            for e in r:
                s[e[1]]+=1
            y_pred.append(np.argmax(s))
        return y_pred, accuracy_score(y_test, y_pred)



