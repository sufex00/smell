from sklearn.model_selection import StratifiedShuffleSplit
from sklearn import preprocessing 
import time

import pandas as pd

print('Carregando dataset')

def start_db(name):
    f = open(name)
    line = f.readline()
    l = 0
    while line: 
        l += 1
        if line[0] is '@':
            line = f.readline()
        else:
            return l-1

def load_data(name, k_fold = 1, dic = '../../db/'):
    name_tra = dic+name+'/'+name+'-10-'+str(k_fold)+'tra.dat'
    name_tst = dic+name+'/'+name+'-10-'+str(k_fold)+'tst.dat'

    data_tra = pd.read_csv(name_tra, sep = ',', header = None, skiprows = start_db(name_tra))
    data_tst = pd.read_csv(name_tst, sep = ',', header = None, skiprows = start_db(name_tst)) 
#
    X_train = data_tra.to_numpy()[:, : -1]
    X_test = data_tst.to_numpy()[:, : -1]
#
    Y_train = data_tra.to_numpy()[:, -1]
    Y_test = data_tst.to_numpy()[:, -1]
#
    label_encoder = preprocessing.LabelEncoder() 
    Y_train = label_encoder.fit_transform(Y_train)
    Y_test  = label_encoder.fit_transform(Y_test)
#
    return np.array(X_train), np.array(Y_train), np.array(X_test), np.array(Y_test), label_encoder


def get_siam_test(X, y, X_test, y_test, num):
#    print('x=', X.shape, y.shape)
#    print('test = ', X_test.shape, y_test.shape)

    s = set()
    x_label = []
    x_test_new = []
    y_label = []

    idx = np.arange(len(X_test))
    np.random.shuffle(idx)

    #print(idx)

    c = 0
    while len(s) < num:
        r = np.random.randint(len(X))
        if r not in s:
            s.add(r)
            x_label.append(X[r])
            x_test_new.append(X_test[idx[c]])
            if y[r] == y_test[idx[c]]:
                y_label.append(1)
            else:
                y_label.append(0)
            c += 1
    return (X_test[idx[:num]], np.array(x_label), np.array(y_label))

def trans(p):
    x = []
    for e in p:
        if e >0.5:
            x.append(1)
        else:
            x.append(0)
    return x

#print(get_siam_test(X_train, y_train, X_test, y_test, 50))

#m = siam()
#m.load_weights("siam_1096.w")

#g = get_siam_test(X_train, y_train, X_test, y_test, 50)

#s = m.predict([g[0], g[1]])

from sklearn.metrics import classification_report, accuracy_score

def label(s):
    y_pred = []

    for idx, e in enumerate(s):
        aux = 0
        if e >0.5:
            aux = 1
        #print('aux=',aux,'real=', g[2][idx])
        y_pred.append(aux)
    return y_pred

#y_pred = label(s)

def get_data(X, y):
    num = set(y)
    num = len(num)
    print(num)
    v = [list() for e in range(num)]
    print(len(y))
    for e in range(len(y)):
        v[y[e]].append(np.array(X[e]))
    num = min([len(v[e]) for e in range(num)])
    return np.array([np.array(e) for e in v])

#v_train = get_data(X, y)
#v_test = get_data(x_test, y_test)

#print(v_test.shape)
#print(v_train.shape)

time.sleep(5)


