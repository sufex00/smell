# Siamese distance metric learning encoder with latent data model for supervised learning (SMELL)


![](../figs/siam_full_schem.jpg)

The left side represents the Encoder with reconstruction, and the right side represents the optimization process for the markers' position for $\mathcal{M} = \{ \bm \mu_1^+,\: \bm \mu_2^+,\: \bm \mu_3^-\}$. In this example, we used two positives and one negative marker. Green and red crosses represent $ \bm \mu_1^+$, $\bm \mu_2^+$, and $\bm \mu_3^-$, respectively, green and red dots represent the similar and dissimilar input pairs. The rightmost green arrow shows a representation of the markers' position optimization step by using  Cross-Entropy divergence and some regularization functions. 
Observe that the number of positive and negative markers are hyperparameters.

```
src
│   README.md   
│
└───monk-2-SMELL
│   │   SMELL(Monk_2).ipynb      # Notebook with example for SMELL training 
│   │   main.py
|   |   keras_smell.py
|   |   preparation.py
│   
└───vehicle-SMELL
|   |   SMELL(vehicle_2).ipynb   # Notebook with example for SMELL training 
│   │   main.py
|   |   keras_smell.py
|   |   preparation.py
|   |   model_plot.pdf           # Encoder architecture used in this the code
```


